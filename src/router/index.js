import Vue from 'vue';
import Router from 'vue-router';

import Login from '../components/LoginRegister/Login';
import Register from "../components/LoginRegister/Register";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: "/Login",
            name: "LoginPage",
            component: Login
        },
        {
            path: '/Register',
            name: 'Register',
            component: Register
        }
    ],
    mode: 'history'
});

export default router;